/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.woramet.cuckoo;

/**
 *
 * @author User
 */
public class Cuckoo {
    private int n = 11;
    public int [ ] CucTable1 = new int [n];
    public int [ ] CucTable2 = new int [n];

    public Cuckoo() {
        
    }

    public int hash1(int key) {
        return key % CucTable1.length;
    }

    public int hash2(int key) {
        return (key / n) % CucTable2.length;
    }

    public int getCucTable1(int key) {
        return CucTable1[hash1(key)];
    }

    public int getCucTable2(int key) {
        return CucTable2[hash2(key)];
    }

    public void cucTable1(int key) {
        int H = hash1(key);
        if(getCucTable1(key) == 0){
            CucTable1[H] = key;
        } else {
            cucTable2(getCucTable1(key));
            CucTable1[H] = key;
        }
    }

    public void cucTable2(int key) {
         int H = hash2(key);
        if(getCucTable2(key) == 0){
            CucTable2[H] = key;
        } else {
            cucTable1(getCucTable2(key));
            CucTable2[H] = key;
        }
    }
    
     public void put (int key){
         cucTable1(key);
    }
}
